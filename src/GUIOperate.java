import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


/**
 * Operator GUI for the user
 */

public class GUIOperate extends javax.swing.JFrame {

    /**
     * Creates new form GUIOperate
     */
    public GUIOperate() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        Thermostat therm = new Thermostat();
        jFrame1 = new javax.swing.JFrame();
        TimeOfDayView = new javax.swing.JLabel();
        DayOfWeekLabel = new javax.swing.JLabel();
        CurrentSettingView = new javax.swing.JLabel();
        TimeOfDaLabel = new javax.swing.JLabel();
        DayOfWeekView = new javax.swing.JLabel();
        CurrentSettingLabel = new javax.swing.JLabel();
        DayOfWeekUp = new javax.swing.JButton();
        DayOfWeekDown = new javax.swing.JButton();
        Save = new javax.swing.JButton();
        Operate = new javax.swing.JButton();
        TimeOfDayUp = new javax.swing.JButton();
        CurrentSettingUp = new javax.swing.JButton();
        TimeOfDayDown = new javax.swing.JButton();
        CurrentSettingDown = new javax.swing.JButton();






        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
                jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
                jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Operator");

        TimeOfDayView.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        TimeOfDayView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TimeOfDayView.setText("00:00 AM");

        DayOfWeekLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        DayOfWeekLabel.setText("Day of the Week");

        CurrentSettingView.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        CurrentSettingView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        CurrentSettingView.setText("75");

        TimeOfDaLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TimeOfDaLabel.setText("Day of the Week");

        DayOfWeekView.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        DayOfWeekView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        DayOfWeekView.setText("Weekend");

        CurrentSettingLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        CurrentSettingLabel.setText("Current Setting");

        DayOfWeekUp.setText("UP");
        DayOfWeekUp.addActionListener(evt -> DayofWeekUpActionPerformed(evt));

        DayOfWeekDown.setText("DOWN");
        DayOfWeekDown.addActionListener(evt -> DayofWeekDownActionPerformed(evt));

        Save.setText("SAVE");
        Save.addActionListener(evt -> SaveActionPerformed(evt));

        Operate.setText("OPERATE");
        Operate.addActionListener(evt -> OperateActionPerformed(evt));

        TimeOfDayUp.setText("UP");
        TimeOfDayUp.addActionListener(evt -> TimeOfDayUpActionPerformed());

        CurrentSettingUp.setText("UP");
        CurrentSettingUp.addActionListener(evt -> CurrentSettingUPActionPerformed());

        TimeOfDayDown.setText("DOWN");
        TimeOfDayDown.addActionListener(evt -> TimeOfDayDownActionPerformed());

        CurrentSettingDown.setText("DOWN");
        CurrentSettingDown.addActionListener(evt -> CurrentSettingDownActionPerformed(evt));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(CurrentSettingView, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(CurrentSettingLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(TimeOfDayView, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addGap(10, 10, 10)
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(TimeOfDaLabel)
                                                                                        .addComponent(DayOfWeekLabel)
                                                                                        .addComponent(DayOfWeekView, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(DayOfWeekUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(TimeOfDayUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(TimeOfDayDown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(DayOfWeekDown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(CurrentSettingDown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(CurrentSettingUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                .addGap(0, 77, Short.MAX_VALUE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                .addGap(0, 0, Short.MAX_VALUE)
                                                                .addComponent(Save, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(Operate)
                                                .addGap(54, 54, 54))))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(DayOfWeekLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(DayOfWeekView, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(13, 13, 13)
                                                .addComponent(TimeOfDaLabel))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(5, 5, 5)
                                                .addComponent(DayOfWeekUp)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(DayOfWeekDown)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(TimeOfDayView, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(32, 32, 32)
                                                .addComponent(CurrentSettingLabel))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(TimeOfDayUp)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(TimeOfDayDown)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(CurrentSettingView, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(CurrentSettingUp)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(CurrentSettingDown)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(Operate)
                                        .addComponent(Save))
                                .addContainerGap())
        );

        pack();
    }// </editor-fold>

    private void DayofWeekUpActionPerformed(java.awt.event.ActionEvent evt) {
        String[] period = {"Holiday", "Weekend"};

        //counts up thru the list of the days of the week and displays them
        boolean up = true;
        int start = Arrays.asList(period).indexOf(DayOfWeekView.getText());


        if((start <= 0) || (start > 6)){

            DayOfWeekView.setText(period[start + 1]);


        }
    }

    private void DayofWeekDownActionPerformed(java.awt.event.ActionEvent evt) {
        String[] period = {"Holiday", "Weekend" +
                ""};

        // counts down thru the days of the week and displays them
        boolean down = true;
        int start = Arrays.asList(period).indexOf(DayOfWeekView.getText());

        if(down && (start > 0 && start <= 7)){
            int total = (start - 1) ;
            DayOfWeekView.setText(period[total]);
            start--;
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() { new GUIOperate1().setVisible(true); }});

        if(DayOfWeekView.getText() == "Holiday"){
            this.dispose();
        }
    }

    private void TimeOfDayUpActionPerformed() {

        String myTime = TimeOfDayView.getText();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm a");
        Date d = null;
        try {
            d = df.parse(myTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.MINUTE, 10);
        String newTime = df.format(cal.getTime());

        TimeOfDayView.setText(newTime);

    }

    private void CurrentSettingUPActionPerformed() {
        Double value = Double.parseDouble(CurrentSettingView.getText());

        if((value < 90) && (value >= 50) ){
            DecimalFormat df = new DecimalFormat("#.#");
            value = Double.parseDouble(CurrentSettingView.getText()) + 1.0;
            String newValue = df.format(value);
            CurrentSettingView.setText( newValue);
        }
    }

    private void TimeOfDayDownActionPerformed() {
        String myTime1 = TimeOfDayView.getText();
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm a");
        Date d1 = null;
        try {
            d1 = df1.parse(myTime1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(d1);
        cal.add(Calendar.MINUTE, -10);
        String newTime = df1.format(cal.getTime());

        TimeOfDayView.setText(newTime);
    }

    private void CurrentSettingDownActionPerformed(java.awt.event.ActionEvent evt) {
        Double value = Double.parseDouble(CurrentSettingView.getText());

        if(value <= 90 && value > 50 ){
            DecimalFormat df = new DecimalFormat("#.#");
            value = Double.parseDouble(CurrentSettingView.getText()) - 1.0;
            String newValue = df.format(value);
            CurrentSettingView.setText(newValue);
        }
    }

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {
        Thermostat therm = new Thermostat();
        TempReadWriter tmp = new TempReadWriter();

        try {
            tmp.write(DayOfWeekView.getText() +"\n   " + TimeOfDayView.getText() + "\n " +  CurrentSettingView.getText() +" degrees\n ");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void OperateActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUIOperate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUIOperate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUIOperate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUIOperate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIOperate().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify
    private javax.swing.JButton CurrentSettingDown;
    private javax.swing.JLabel CurrentSettingLabel;
    private javax.swing.JButton CurrentSettingUp;
    private javax.swing.JLabel CurrentSettingView;
    private javax.swing.JButton DayOfWeekDown;
    private javax.swing.JLabel DayOfWeekLabel;
    private javax.swing.JButton DayOfWeekUp;
    private javax.swing.JLabel DayOfWeekView;
    private javax.swing.JButton Operate;
    private javax.swing.JButton Save;
    private javax.swing.JLabel TimeOfDaLabel;
    private javax.swing.JButton TimeOfDayDown;
    private javax.swing.JButton TimeOfDayUp;
    private javax.swing.JLabel TimeOfDayView;
    private javax.swing.JFrame jFrame1;
    // End of variables declaration
}
