
import java.io.*;
import java.util.Scanner;



public class TempReadWriter {



    TempReadWriter() {

    }


    // read information from file
    public void read(){

        System.out.println();
        System.out.println("DATA READ FROM FILE WITH SAVED INFORMATION");

        String fileName = "temperatureValues.txt";


        try {
            // FileReader reads text files in the default encoding.
            File file =
                    new File(fileName);
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }

        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
    }// end read method


    // write information to file
    public void write(String data) throws IOException{

        File file = new File("UserSettings.txt");
        PrintWriter writer = new PrintWriter(new FileWriter(file, false));
        writer.println(data);
        writer.println();

        writer.close();

    }





}
