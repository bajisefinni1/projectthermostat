public class Thermostat {

    private String nameID;
    private String name;
    private String thermostatTime;
    private Double thermostatTemp;
    private Double humdity;

    Thermostat(){
        this.thermostatTemp = 0.0;
        this.humdity = 0.0;

    }

    Thermostat(String nameID, String name, String thermostatTime){
        this.name = name;
        this.nameID = nameID;
        this.thermostatTime = thermostatTime;

    }



    public String GetTime(){
        return thermostatTime;
    }

    public void SetTime(String value){
        this.thermostatTime = value;
    }

    public String thermostatName(){
        return name;
    }

    public void SetName(String value){
        this.name = value;

    }

    public String GetName(){
        return name;
    }

    public String GetNameID(){
        return nameID;
    }

    public void SetNameID(String value){
        this.nameID = value;
    }

    public Double GetThermostatTemp(){

        return thermostatTemp;

    }

    public void SetThermostatTemp(Double value){

        this.thermostatTemp = value;

    }

    public void SetHumidity(Double value){
        this.humdity = value;
    }

    public double GetHumidity(){
        return humdity;
    }

    public String ToString(){
        return " \nName: " + GetName() + " \n ID: " + GetNameID() +
                " \nTime: " + GetTime() + " \nCurrent Temperature: " + GetThermostatTemp()
                + " \nHumidity: " + GetHumidity() + "\n";
    }



}
